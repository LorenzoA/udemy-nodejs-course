/*  Server creation */
const express = require('express')

const morgan = require('morgan')
const favicon = require('serve-favicon')
const bodyParser = require('body-parser')
const sequelize = require('./src/db/sequelize')

const app = express()
const port = 3000


app     // middleware, used by the app on all endpoints
    .use(morgan('dev'))
    .use(favicon(__dirname + '/favicon.ico'))
    .use(bodyParser.json())

sequelize.initDb()

// futur endpoints

/*
const findAllPokemons = require('./src/routes/findAllPokemons')  

findAllPokemons(app)  
*/                               

require('./src/routes/findAllPokemons')(app) // refactorisation du code d'au dessus
require('./src/routes/findPokemonByPk')(app) 
require('./src/routes/createPokemon')(app) 
require('./src/routes/updatePokemon')(app) 
require('./src/routes/deletePokemon')(app)
require('./src/routes/login')(app)  


//Erorr 404 handling

app.use(({res}) =>{
    const message = "Impossible de trouver la ressource demandée !"
    res.status(404).json({message})
} )


app.listen(port, () => console.log(`Notre application Node est démarrée sur : http://localhost:${port}`))



