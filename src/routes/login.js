const { User } = require('../db/sequelize')
const bcrypt = require('bcrypt')
const jwt =  require('jsonwebtoken')
const privateKey = require('../auth/private_key')

module.exports = (app) => {
    app.post('/api/login', (req, res) => {

        User.findOne({ where: { username: req.body.username } }).then(user => {

            if(!user){
                const message = 'Utilisateur demandé non existant'
                res.status(404).json(message)
            }

            bcrypt.compare(req.body.password, user.password).then(isPasswordValid => { // comparaison des hachages dans la bdd
                if (!isPasswordValid) {
                    const message = `Mauvais MDP`;
                    return res.status(401).json({ message})
                }

                const token =  jwt.sign( // Methode sign de JWT
                    {userId:user.id},
                    privateKey,
                    {expiresIn: '24h'}
                )

                const message = 'Utilisateur connecté'
                res.json({ message, data: user, token })
            })
        })
        .catch(error => {
            const message = 'Utilisateur na pas pu etre connecté, reesayez'
                res.status(404).json(message)
        })
    })
}