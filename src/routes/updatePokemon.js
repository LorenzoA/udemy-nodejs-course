const { Pokemon } = require('../db/sequelize')

module.exports = (app) => {
    app.put('/api/pokemons/:id', (req, res) => {
        const id = req.params.id
        Pokemon.update(req.body, {
            where: { id: id }
        })
            .then(_ => {
                Pokemon.findByPk(id).then(pokemon => {

                    if (pokemon === null) {

                        const message = "Le pokémon demandé n'existe pas"
                        return res.statut(404).json({ message, data: pokemon })
                    }

                    const message = `Le pokémon ${pokemon.name} a bien été modifié.`
                    res.json({ message, data: pokemon })
                })
                    .catch(error => {
                        
                        if(error instanceof ValidationError) {
                            return res.status(400).json({ error: error.message, data: error})
                        }
                        const message = "Recupération échouée réessayez plus tard ..."
                        res.status(500).json({ message, error })
                    })
                    .catch(error => {
                        if(error instanceof ValidationError) {
                            return res.status(400).json({ error: error.message, data: error})
                        }
                        const message = "Recupération échouée réessayez plus tard ..."
                        res.status(500).json({ message, error })
                    })
            })
    })
}