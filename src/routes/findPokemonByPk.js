const { Pokemon } = require('../db/sequelize')

module.exports = (app) => {
    app.get('/api/pokemons/:id', (req, res) => {
        Pokemon.findByPk(req.params.id)
            .then(pokemon => {

                if (pokemon === null) {
                    const message = "Le pokémon demandé n'existe pas"
                    res.statut(404).json({ message, data:pokemon})
                }
                const message = 'Un pokémon a bien été trouvé.'
                return res.json({ message, data: pokemon })
            })
            .catch(err => {
                const message = "Recupération échouée réessayez plus tard ..."
                res.status(500).json({ message, err })
            })
            
    })
}