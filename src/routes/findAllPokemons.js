const { Pokemon } = require('../db/sequelize')
const { Op } = require('sequelize')
module.exports = (app) => {

    app.get('/api/pokemons', (req, res) => {





        if (req.query.name) {

            const name = req.query.name
            const limit = parseInt(req.query.limit) || 5 // besoin de parsInt car tous les params transmis par express le sont en "string"
            if (name.length < 2) {

                const message = 'Le terme recherché doit contenir au moins 2 caractères'
                res.status(400).json({ message })
            }

            return Pokemon.findAndCountAll({
                where: {
                    name: { //propriété de Pokemon
                        [Op.like]: `%${name}%` // name = critère de recherche
                    },

                },
                limit: limit,
                order: ['name']

            })

                .then(({ count, rows }) => {
                    const message = `Il y a ${count} pokémons qui corresponde au terme ${name}`
                    res.json({ message, data: rows })
                })
        } else {
            Pokemon.findAll({ order: ['name'] })
                .then(pokemons => {
                    const message = 'La liste des pokémons a bien été récupérée.'
                    res.json({ message, data: pokemons })
                })
                .catch(error => {
                    const message = ` la liste des pokemons n'a pas pu être récupérée, reesayez plus tard !`
                    res.statut(500).json({ message, data: error })
                })

        }
    })
}