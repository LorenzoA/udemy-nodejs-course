const validTypes = ['Plante', 'Poison', 'Feu', 'Eau', 'Insecte', 'Vol', 'Normal', 'Electrik', 'Fée']

module.exports = (sequelize, DataTypes) => {
    return sequelize.define('Pokemon', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            unique:{
                msg: 'Le nom est déjà pris'
            },
            validate: {
                notEmpty: { msg: 'The name is required' },
                notNull: { msg: 'Les points de vie sont une propriété requise' },
                min: {
                    args: [1],
                    msg: "Le nom ne peut pas faire moins de 1 caractères"
                },
                max: {
                    args: [125],
                    msg: "Le nom ne peut pas faire plus de 125 caractères"
                },
            }
        },
        hp: {
            type: DataTypes.INTEGER,
            allowNull: false,
            validate: {
                isInt: { msg: 'Utilisez uniquement des nombres entiers pour les PDV' },
                notNull: { msg: 'Les points de vie sont une propriété requise' },
                min: {
                    args: [0],
                    msg: "Les hp ne peuvent pas etre en dessous de zero"
                },
                max: {
                    args: [999],
                    msg: "Les hp ne peuvent pas etre en dessus de 999"
                },
            }
        },
        cp: {
            type: DataTypes.INTEGER,
            allowNull: false,
            validate: {
                isInt: { msg: 'Utilisez uniquement des nombres entiers pour les CP' },
                notNull: { msg: 'Les CP sont une propriété requise' },
                min: {
                    args: [0],
                    msg: "Les cp ne peuvent pas etre en dessous de zero"
                },
                max: {
                    args: [99],
                    msg: "Les cp ne peuvent pas etre en dessus de 99"
                },

            }
        },
        picture: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                isUrl: {
                    msg: 'Utilisez uniquement des url pour L\'image'
                },


                notNull: { msg: 'L\'image est une propriété requise' }
            }
        },
        types: {
            type: DataTypes.STRING,
            allowNull: false,
            get() {
                return this.getDataValue('types').split(',');
            },
            set(types) {
                this.setDataValue('types', types.join())
            },
            validate: {
                //Validateur personalisé
                isTypesValid(value) {
                    if(!value){
                        throw new Error('Un pokemon doit au moins avoir un type')
                    }
                    if(value.split(',').length > 3){
                        throw new Error('Un popkemon ne peut avoir plus de 3 types')
                    }
                    value.split(',').forEach(types => {
                        if (!validTypes.includes(types)){
                                throw new Error(`Invalid type !!!!!!! ${validTypes} `)
                                
                        } 
                            
                       console.log(value.split(','));
                    });
                }
            }
        }
    }, {
        timestamps: true,
        createdAt: 'created',
        updatedAt: false
    })
}